@extends('master')
@section('content')
<!-- <div class="content-header">
  <h3 style="color:black;font-family: 'Abel', sans-serif;font-size: 250%;"> <i class="fa  fa-spinner"></i>&nbsp;&nbsp;&nbsp;Add New Customer </h3><br><br>
</div> -->
	<div class="box">
		
		<div class="row">
			<div class="col-md-3">
				<b> <h4 style="color:;black;"> &nbsp;ORDERS </h4></b>
			</div>
			<div class="col-md-3">
				<h4><a href="" style="color:grey;">  All Orders </a></h4>
			</div>
			<div class="col-md-3">
				<h4><a href="" style="color:grey;">  Awaiting Shipment </a></h4>
			</div>
			<div class="col-md-3">
				<h4><a href="" style="color:grey;">  Awaiting Approval </a></h4>
			</div>
			<hr>
		</div>
<hr>
		<div class="row">
		  <div class="box-body">
		<!-- 	<div class="col-md-1" style="margin-top:2px;"> -->
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/turn-down.png">
	<!-- 		</div> -->

			<div class="btn-group">
                  <button type="button" class="btn btn-default">Bulk Action</button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Confirm Order</a></li>
                    <li><a href="#">Cancel Order</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Import Orders</a></li>
                    <li><a href="#">Export Orders</a></li>
                  </ul>
            </div>

            <div class="btn-group">
                  <button type="button" class="btn btn-default">Print</button>
                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Quote Summary</a></li>
                    <li><a href="#">Purchase Summary</a></li>
                  </ul>
            </div>

            <button type="button" id="btnAddOrder" data-toggle="modal" data-target="#add_order_modal" class="btn bg-blue btn-flat margin"><i class="fa fa-plus"></i>  Add Order &nbsp;</button> 

            <div class="modal fade" style="margin-top:50px" id="orderModal" role="dialog">
		     	 <div class="col-md-8 col-md-offset-2">
		          	<div class="modal-content">
			            <div class="modal-header">
			              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                <span aria-hidden="true">&times;</span></button>
			                <center>
			              <h4 class="modal-title"> Add Order </h4> </center>
			            </div>
				            <form class="" id="0rder_form" role="form" data-toggle="validator">

					            <div class="modal-body">
					            	<div class="box-body">
					            		<div class="row">

						            		<div class="col-md-6">
						            			<div class="form-group">
						            				<label> Order No. </label>
						            				<input type="text" id="order_num" class="form-control" disabled>

						            			</div>
						            		</div>

						            		<div class="col-md-6">
						            			<div class="form-group">
								               		 <label>Order Date:</label>
							               				 <div class="input-group date">
											                 <div class="input-group-addon">
											                   <i class="fa fa-calendar"></i>
											                 </div>
									                  		<input type="text" class="form-control pull-right" id="datepicker">
									                	</div>
								              	</div>
						            		</div>

						            	</div>
						            	<div class="row">
						            		<div class="col-md-6">
											  <div class="form-group has-feedback">
											    <div class="form-group">
											      <label for="companyName" class="control-label">Company Name</label>
											      <input type="text" class="form-control validate letter" id ="companyName" required
											      data-error="Company name is required."
											      data-minlength-error="Minimum length 6."
											      data-minlength="2"
											      maxlength="35">
											      <div class="help-block with-errors"></div>
											      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
											    </div>
										      </div>
									        </div>

						            		<div class="col-md-6">
							            		<label> Shipping Address </label>
								            		<div class="form-group">
								            			<textarea class="form-control" rows="2">
								            			</textarea>
								            		</div>
						            		</div>
						            	</div>

						            	<div class="row">
						            		<div class="col-md-6">
											  <div class="form-group has-feedback">
											    <div class="form-group">
											      <label for="contactPerson" class="control-label">Contact person</label>
											      <input type="text" class="form-control validate letter" id ="custName" required
											      data-error="Contact person is required."
											      data-minlength-error="Minimum length 6."
											      data-minlength="2"
											      maxlength="35">
											      <div class="help-block with-errors"></div>
											      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
											    </div>
										      </div>
									        </div>

						            		<div class="col-md-6">
						            			<div class="form-group">
						            				<label> Payment Term </label>
						            					<select class="form-control select2" style="width:100%">
								            				 <option selected disabled>Select Payment Term</option>
										                  	 <option>On Delivery</option>
										                   	 <option>30 days after Delivery</option>
										                </select> 
						            			</div>
						            		</div>
						            	</div>

						            	<div class="row">
						            		<div class="col-md-6">
						            			<div class="form-group">
						            				<label> Assigned Employee </label>
						            					<select id="emp" class="form-control select2" style="width:100%">
						            						<option selected disabled> Select Employee </option>
						            						<option> Paul Cruz </option> 
						            						<option> Alfred Clave </option> 
						            						<option id="../maintenance/employee" value="shit" style="color:blue"> + &nbsp; Add New Employee</option>
						            					</select>
						            			</div>
						            		</div>
						            	</div>
						            		
					            	</div>
					            </div>

					            <div class="modal-footer">
						             <button type="submit" class="btn btn-info pull-left"><i class="glyphicon glyphicon-ok"></i> &nbsp;Save</button>
						             <button type="reset" class="btn btn-default pull-left"><i class="fa fa-pencil"></i> &nbsp;Clear</button>
					            </div>
				            </form>
		         	 </div>
		        </div>
     		 </div>
       	</div>
    </div>

	 <div class="box-body">
	   <table id="orders" class="display select" cellspacing="0" width="100%">
	   		<thead>
			      <tr height="10px">
			         <th><input type="checkbox" name="select_all" value="1" id="order-select-all"></th>
			         <th>Order No.</th>
			         <th>Customer Name</th>
			         <th>Shipping City</th>
			         <th>Status</th>
			         <th>Invoiced</th>
			         <th>Paid</th>
			         <th>Order Date</th>
			      </tr>
	   		</thead>
	   		<tbody>
	   			<tr>

	   				<td> <input type="checkbox" name="select-first" value="1" id="order-select-first"> </td> 
	   				<td> ORDER101 </td>
	   				<td> Rexielyn Santos </td>
	   				<td> Pasig </td>
	   				<td> 
	   					<span class="pull-right-container">
	  						<small class="label bg-green">CONFIRMED</small>
						</span>
					<td> 
						<span class="pull-right-container">
	  						<small class="label bg-blue">INVOICED</small>
						</span>
					</td>
					<td> 
						<span class="pull-right-container">
	  						<small class="label bg-orange">PAID</small>
						</span>
					</td>
					<td> asdf </td>

	   			</tr>

	   			<tr>

	   				<td> <input type="checkbox" name="select-first" value="1" id="order-select-first"> </td> 
	   				<td> ORDER102 </td>
	   				<td> Polene Afable </td>
	   				<td> Mandaluyong </td>
	   				<td> 
	   					<span class="pull-right-container">
	  						<small class="label bg-green"> CONFIRMED</small>
						</span>
					<td> 
						<span class="pull-right-container">
	  						<small class="label bg-blue">INVOICED</small>
						</span>
					</td>
					<td> 
						<span class="pull-right-container">
	  						<small class="label bg-red">UNPAID</small>
						</span>
					</td>
					<td> asdf </td>

	   			</tr>
	   		</tbody>
	   </table>
	</div>


        <div class="box-footer">
		     <a href=""> <button type="button" class="btn bg-blue btn-flat margin pull-right">Cancel &nbsp;</button> </a>
		     <a href="/transaction/orders"> <button type="submit" class="btn bg-blue btn-flat margin pull-right">Save & Add New &nbsp;</i></button> </a>
		     <a href="/transaction/orders"> <button type="submit" class="btn bg-blue btn-flat margin pull-right">Save &nbsp;</button> </a>
      	</div>

	</div>
	   
	



@stop

@section('script')
<script>
$(document).on('click', '#btnAddOrder', function(){
	$('#orderModal').modal('show');
});

$(document).on('change', '#emp', function(){
	if(this.value=="shit"){
		window.location.href = "../maintenance/employee";
	}
});

$(document).on('change', '#cust', function(){
	if(this.value=="shit"){
		window.location.href = "../transaction/customer";
	}
});

$(document).ready(function (){
   var table = $('#orders').DataTable(
   	);

   // Handle click on "Select all" control
   $('#order-select-all').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });

   // Handle click on checkbox to set state of "Select all" control
   $('#orders tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#order-select-all').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control 
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
   });

   // Handle form submission event
   $('#frm-example').on('submit', function(e){
      var form = this;

      // Iterate over all checkboxes in the table
      table.$('input[type="checkbox"]').each(function(){
         // If checkbox doesn't exist in DOM
         if(!$.contains(document, this)){
            // If checkbox is checked
            if(this.checked){
               // Create a hidden element 
               $(form).append(
                  $('<input>')
                     .attr('type', 'hidden')
                     .attr('name', this.name)
                     .val(this.value)
               );
            }
         } 
      });
   });

});

</script>
@stop