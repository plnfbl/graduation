@extends('master')
@section('pageTitle', 'Quote Requests')
@section('content')

	<div class="box box-info">
	<div class="row">
		<div class="col-md-4">
			<b> <h4 style="color:;black;"> &nbsp; Quote Requests </h4></b>
		</div>
	</div><hr>
	<div class="row">
	  <div class="box-body">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<!-- &nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/turn-down.png"> -->
			<div class="btn-group">
	            <button type="button" class="btn btn-default">Bulk Action</button>
	            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
		            <span class="caret"></span>
		            <span class="sr-only">Toggle Dropdown</span>              
		        </button>
            		<ul class="dropdown-menu" role="menu">
				        <li><a href="#">Confirm Quote</a></li>
				        <li><a href="#">Cancel Quote</a></li>
				        <li class="divider"></li>
				        <!-- <li><a href="#">Import Orders</a></li> -->
				        <li><a href="#">Export Quotes</a></li>
		            </ul>
        	</div>

	        <div class="btn-group">
                <button type="button" class="btn btn-default">Print</button>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
	                <span class="caret"></span>
	                <span class="sr-only">Toggle Dropdown</span>
	            </button>
	                <ul class="dropdown-menu" role="menu">
		                <li><a href="#">Order Estimate</a></li>
		                <li><a href="#">Quote Summary</a></li>
	                </ul>
	        </div>

	        <button id="btnAddQuote" class="btn bg-blue btn-flat margin"><i class="fa fa-plus"></i>  Add New Quote&nbsp;</button> 
   		</div>
	</div>

 	<div class="box-body">
  		 <table id="quoteTable" class="display select" cellspacing="0" width="100%">
   			<thead>
			      <tr height="10px">
			         <th><input type="checkbox" name="select_all" value="1" id="quotes-select-all"></th>
			         <th>Order No.</th>
			         <th>Customer Name</th>
			         <th>Order Details</th>
			         <th>Order Date</th>
			         <th>Status</th>
			      </tr>
   			</thead>
   			<tbody>
   				<tr>
   			
	   				<td> <input type="checkbox" name="select-first" value="1" id="quote-select-first"> </td> 
	   				<td> ID</td>
	   				<td> Toyota </td>
	   				<td> 
	   					<a href=""> View Order Details </a>
	   			    </td>
	   				<td> 
	   					12/24/2017
					</td> 
					<td>
						<span class="pull-right-container">
	  						<small class="label bg-blue">Approved</small>
						</span>
					</td>
			
   				</tr>
   			</tbody>
   		</table>
	</div>
</div>
	

@push('scripts')
 <script type="text/javascript" src="{{URL::asset('js/logic/estimate.js')}}"></script>
@endpush
@stop