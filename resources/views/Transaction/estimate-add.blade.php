@extends('master')
@section('pageTitle', 'Quote Request')
@section('content')
	
<!-- <h3 style="color:black;font-family: 'Abel', sans-serif;font-size: 180%;">Order Estimate </h3> -->
<form class="" id="estimate_form" role="form" data-toggle="validator">
	<div class="box box-danger">
		<div class="box-header with-border">
	          <h3 class="box-title">Customer Information</h3>

	          <div class="box-tools pull-right">
	            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	            <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
	          </div>
	    </div>
			<div class="box-body">
			
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-12">
							 	<div class="form-group has-feedback">
									<div class="form-group">
								         <label for="companyName" class="control-label">Company Name</label>
								          <input type="text" class="form-control validate" id ="companyName" placeholder="Company Name" 
								          data-minlength-error="Minimum length 2."
								          data-minlength="2"
								          maxlength="35" style="border:1px solid #A9A9A9">
								          <div class="help-block with-errors"></div>
								          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
						        	</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group has-feedback">
									<div class="form-group">
										<label for="streetNo" class="control-label">Address<span style="color:red">*</span></label>
								          <input type="text" class="form-control validate" id ="streetNo" placeholder="No/Street" 
								          data-minlength-error="Minimum length 5."
								          data-minlength="2"
								          maxlength="35" required style="border:1px solid #A9A9A9">
								          <div class="help-block with-errors"></div>
								          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								     </div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">	
								<div class="form-group has-feedback">
									<div class="form-group">
							          <input type="text" class="form-control validate" id ="brgy" placeholder="Barangay/Subdivision" 
							          data-minlength-error="Minimum length 5."
							          data-minlength="2"
							          maxlength="35" required style="border:1px solid #A9A9A9">
							          <div class="help-block with-errors"></div>
							          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							         </div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="from-group has-feedback">
									<div class="form-group">
							          <input type="text" class="form-control validate" id ="city" placeholder="City" 
							          data-minlength-error="Minimum length 5."
							          data-minlength="2"
							          maxlength="35" required style="border:1px solid #A9A9A9">
							          <div class="help-block with-errors"></div>
							          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							         </div>
							     </div>
							</div>
						</div>

					</div>

					<div class="col-md-6">

						<div class="row">
							<div class="col-md-12">
								<div class="form-group has-feedback">
									<div class="form-group">
										<label for="contactPersonName" class="control-label">Contact Person<span style="color:red">*</span></label>
								          <input type="text" class="form-control validate" id ="contactPersonName" placeholder="Name" 
								          data-minlength-error="Minimum length 5."
								          data-minlength="2"
								          maxlength="35" style="border:1px solid #A9A9A9; width:98%;" required>
								          <div class="help-block with-errors"></div>
								          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							         </div>
							    </div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group has-feedback">
									<div class="form-group">
										<label for="contactNo" class="control-label">Contact Number<span style="color:red">*</span></label>
							        	<div class="input-group margin">
							                <div class="input-group-btn">
							                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-phone-square"></i>
							                    <span class="fa fa-caret-down"></span></button>
							                  <ul class="dropdown-menu">
							                    <li><a id="btncellphone" href="#">Cellphone Number</a></li>
							                    <li><a id="btnhomephone" href="#">Home Phone</a></li>
							                    <li><a id="btnworkphone" href="#">Work Phone</a></li>
							                  </ul>
							                </div>
						                <input type="text" class="form-control" id ="emp_contact" name="emp_contact" placeholder="ex.09123456789" required
                        title="09*********" data-inputmask='"mask": "(99) 999-999-999"' data-mask
                        data-error="Contact number is required." required>
						             	</div>
						             </div>
						        </div>
		             		</div>
						</div>
					</div>
				</div>
			</div>
	</div>



	<div class="box box-primary">
		<div class="box-header with-border">
	          <h3 class="box-title">Order Details</h3>

	          <div class="box-tools pull-right">
	            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          </div>
	    </div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
			                       <label for="productName" class="control-label">Product Name<span style="color:red">*</span></label>
			                         <select id="prodSelect" name="prodSelect" class="form-control select2" multiple="multiple" data-placeholder="Select Products" style="width: 100%;border:1px solid #A9A9A9">
			                         </select>
			                        <!--  <small> <a href="../maintenance/product"> + Add New Product </a></small> -->
			                      </div>
							</div>
						</div>
					</div>
				</div>
<hr>
				<div class="row">
					<div class="col-md-12">
				      <table  id="orderDetailTable" name="orderDetailTable" class="display" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				                <th class="hidden">ID</th>
				                <th>Image</th>
				                <th>Product Name</th>
				                <th>Variant(s)</th>
				                <th> Remarks </th>
				            </tr>
				        </thead>
				        <tfoot>
				            <tr>
				                <th class="hidden">ID</th>
				                <th>Image</th>
				                <th>Product Name</th>
				                <th>Variant(s)</th>
				                <th> Remarks </th>
				            </tr>
				        </tfoot>
				        <tbody>
				            <tr>
				                <td class="hidden">QR00001</td>
				                <td><img src="../images/queries-icon.png"></td>
				                <td>PV-025 B</td>
				                <td>
									  <select class="form-control select2" id="orderProdVar" multiple="multiple" data-placeholder="Select Variants" style="width: 100%;border:1px solid #A9A9A9" >
				                        <option>100pcs</option>
				                        <option>200pcs</option>
				                        <option>300pcs</option>
				                     </select>
				                </td>
				                <td> <input type="text" id="remarks"> </td>
				            </tr>
				        </tbody>
				       </table>
  					 </div>
				</div>
				<hr>
				<div class="row">
				<div class="col-md-12">
					<button type="reset" class="btn bg-white btn-flat pull-right"><i class="fa fa-pencil"></i> &nbsp;Clear</button>
                <button type="submit" class="btn bg-blue btn-flat pull-right"><i class="glyphicon glyphicon-ok"></i> &nbsp;Submit</button>
                </div>
				</div>
			</div>
		</div>
	</form>

@push('scripts')
 <script type="text/javascript" src="{{URL::asset('js/logic/estimate.js')}}"></script>
<script>
$(document).ready(function() {
            $('#orderDetailTable').DataTable();
        });
</script>
@endpush
@stop
