@extends('master')
@section('pageTitle', 'Quote Request')
@section('content')
	
<!-- <h3 style="color:black;font-family: 'Abel', sans-serif;font-size: 180%;">Order Estimate </h3> -->

	<div class="box box-danger">
		<div class="box-header with-border">
	          <h3 class="box-title">Customer Information</h3>

	          <div class="box-tools pull-right">
	            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	            <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
	          </div>
	    </div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-12">
							 	<div class="form-group has-feedback">
									<div class="form-group">
								         <label for="companyName" class="control-label">Company Name</label>
								          <input type="text" class="form-control validate" id ="companyName" placeholder="Company Name" 
								          data-minlength-error="Minimum length 2."
								          data-minlength="2"
								          maxlength="35" style="border:1px solid #A9A9A9">
								          <div class="help-block with-errors"></div>
								          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
						        	</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group has-feedback">
									<div class="form-group">
										<label for="streetNo" class="control-label">Address<span style="color:red">*</span></label>
								          <input type="text" class="form-control validate" id ="streetNo" placeholder="No/Street" 
								          data-minlength-error="Minimum length 2."
								          data-minlength="2"
								          maxlength="35" style="border:1px solid #A9A9A9">
								          <div class="help-block with-errors"></div>
								          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								     </div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">	
								<div class="form-group has-feedback">
									<div class="form-group">
							          <input type="text" class="form-control validate" id ="brgy" placeholder="Barangay/Subdivision" 
							          data-minlength-error="Minimum length 5."
							          data-minlength="2"
							          maxlength="35" style="border:1px solid #A9A9A9">
							          <div class="help-block with-errors"></div>
							          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							         </div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="from-group has-feedback">
									<div class="from-group">
							          <input type="text" class="form-control validate" id ="city" placeholder="City" 
							          data-minlength-error="Minimum length 5."
							          data-minlength="2"
							          maxlength="35" style="border:1px solid #A9A9A9" required>
							          <div class="help-block with-errors"></div>
							          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							         </div>
							     </div>
							</div>
						</div>

					</div>

					<div class="col-md-6">

						<div class="row">
							<div class="col-md-12">
								<div class="form-group has-feedback">
									<div class="form-group">
										<label for="contactPersonName" class="control-label">Contact Person<span style="color:red">*</span></label>
								          <input type="text" class="form-control validate" id ="contactPersonName" placeholder="Name" 
								          data-minlength-error="Minimum length 5."
								          data-minlength="2"
								          maxlength="35" style="border:1px solid #A9A9A9; width:98%;" required>
								          <div class="help-block with-errors"></div>
								          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							         </div>
							    </div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="form-group has-feedback">
									<div class="form-group">
										<label for="contactNo" class="control-label">Contact Number<span style="color:red">*</span></label>
							        	<div class="input-group margin">
							                <div class="input-group-btn">
							                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-phone-square"></i>
							                    <span class="fa fa-caret-down"></span></button>
							                  <ul class="dropdown-menu">
							                    <li><a id="btncellphone" href="#">Cellphone Number</a></li>
							                    <li><a id="btnhomephone" href="#">Home Phone</a></li>
							                    <li><a id="btnworkphone" href="#">Work Phone</a></li>
							                  </ul>
							                </div>
						                <input type="text" class="form-control">
						             	</div>
						             </div>
						        </div>
		             		</div>
						</div>
					</div>
				</div>
			</div>
	</div>



	<div class="box box-primary">
		<div class="box-header with-border">
	          <h3 class="box-title">Order Details</h3>

	          <div class="box-tools pull-right">
	            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	          </div>
	    </div>
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
			                       <label for="variantName" class="control-label">Product Name<span style="color:red">*</span></label>
			                         <select class="form-control select2" multiple="multiple" data-placeholder="Select Products" style="width: 100%;border:1px solid #A9A9A9" >
			                            <option>PV-025</option>
			                            <option> NT-2047  </option>
			                            <option> TR-5B </option>
			                         </select>
			                         <small> <a href="../maintenance/productVariant"> + Add New Product </a></small>
			                      </div>
							</div>
						</div>
					</div>

					<div class="col-md-6">

						
					</div>
				</div>
<hr>
				<div class="row">
					<div class="col-md-12">
      <table  id="orderTable" name="productTable" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="hidden">ID</th>
                <th>Image</th>
                <th>Product Name</th>
                <th>Type</th>
                <th>Variant</th>
                <th>Quantity</th>
                <th> Remarks </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                 <th class="hidden">ID</th>
                 <th>Image</th>
                <th>Product Name</th>
                <th>Type</th>
                <th>Variant</th>
                <th>Quantity</th>
                <th> Remarks </th>
            </tr>
        </tfoot>
        <tbody>
            <tr>
                <td class="hidden">PROD00001</td>
                <td><img src="../images/queries-icon.png"></td>
                <td>PV-025 B</td>
                <td>
                	 <div class="form-group has-feedback">
                  		<div class="form-group">

		                    <select class="form-control select2" id = "uomTypeSelect" style="width: 100%;" required>
		             
		                      <option value="">Investment Casting</option>
      
                    		</select>
                    		<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                 		 </div>
               		 </div>
                </td>
                <td>
                  <div class="form-group has-feedback">
                  		<div class="form-group">
							  <select class="form-control select2" multiple="multiple" data-placeholder="Select Variants" style="width: 100%;border:1px solid #A9A9A9" >
	                            <option>PV-025</option>
	                            <option> NT-2047  </option>
	                            <option> TR-5B </option>
	                         </select>
                 		 </div>
               		 </div>
                </td>
                <td> <input type="number"> </td>
                <td> gsdgsdgsdg </td>

            </tr>
          </tbody>
    </table>
    </div>
				</div>
			</div>
	</div>

@push('scripts')
<script>
$(document).ready(function() {
            $('#orderTable').DataTable();
        } );
</script>
@endpush
@stop
