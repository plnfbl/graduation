$(document).ready(function(){
  var table =  $('#jobTitleTable').DataTable();
  var urlCode = '';
  var tempID = '';

$("#btnAddJob").click(function(){
  $("#JobTitle_form").find('.has-error').removeClass("has-error");
  $("#JobTitle_form").find('.has-success').removeClass("has-success");
  $('#JobTitle_form').find('.form-control-feedback').remove();
  // $("#modDept").val(null).change();
  document.getElementById("JobTitle_form").reset();
  document.getElementById('JobTitle_form').action = "{{URL::to('/maintenance/jobTitle-add')}}";
  urlCode =  '/maintenance/jobTitle-add';
})

$("#btnEditJob").click(function(){
  $("#JobTitle_form").find('.has-error').removeClass("has-error");
  $("#JobTitle_form").find('.has-success').removeClass("has-success");
  $('#JobTitle_form').find('.form-control-feedback').remove()
  document.getElementById("JobTitle_form").reset();
  var tblData = table.row('tr.active').data();
  var id = tblData[0];
  $.ajax({
      url: '/maintenance/jobTitle-edit',
      type: 'POST',
      data: {
        jobtitle_id: id
      },
      success: function(data)
      {
        // CHANGE ADD THIS DEPENDS ON INPUT FIELDS
       $('#JobTitleDesc').val(data[0].strJobTitleDesc);
        $('#JobTitleName').val(data[0].strJobTitleName);
        // URL OF EDIT
        tempID = data[0].strJobTitleID;
        document.getElementById('JobTitle_form').action = "{{URL::to('/maintenance/jobTitle-update')}}";
        urlCode =  '/maintenance/jobTitle-update';
      },
      error: function(result) {
        alert('No ID found!');
      }
  });
})


  $(document).on('submit', '#JobTitle_form', function(e){
    table.column(0).visible(false);
    e.preventDefault();
      $.ajax({
        type: "POST",
        url: urlCode,
        data: {
            jobtitle_desc: $('#JobTitleDesc').val(),
            jobtitle_name: $('#JobTitleName').val(),
            jobtitle_id: tempID
        },
        success: function(result) {
          if(urlCode == '/maintenance/jobTitle-update'){
            table.rows('tr.active').remove().draw();
            noty({
              type: 'success',
              layout: 'bottomRight',
              timeout: 3000,
              text: '<h4><center>Job Title successfully updated!</center></h4>',
            });
          }
          else{
            noty({
              type: 'success',
              layout: 'bottomRight',
              timeout: 3000,
              text: '<h4><center>Job Title successfully added!</center></h4>',
            });
          }
          table.row.add([
            result[0].strJobTitleID,
            result[0].strJobTitleName,
            result[0].strJobTitleDesc,
            ]
          ).draw(false);

          $('#JobTitleName').val('')
          $('#JobTitleDesc').val('')
          $('#JobTitleModal').modal('toggle');
          $('#btnEditJob').hide()
          $('#btnDeleteJo').hide()
          $('#btnAddJob').show()
        },
        error: function(result){
          var errors = result.responseJSON;
          if(errors == undefined){
            noty({
              type: 'error',
              layout: 'bottomRight',
              timeout: 3000,
              text: '<h4><center>Job Title name already exist!</center></h4>',
            });
          }
        }
      });

  })


$('#btnDeleteJob').click(function(){
  var tblname = $('#jobTitleTable').DataTable();
  var selected = tblname.rows('tr.active').data();
  var selectedArr = [];

  for(var i = 0; i < selected.length; i++)
    {
       selectedArr[i] = selected[i][0];
    }

  $.ajax({
    type: "POST",
    url: "/maintenance/jobTitle-delete",
    data: {
        jobtitle_id: selectedArr
    },
    success: function(result) {
      tblname.rows('tr.active').remove().draw();
      $('#JobTitleDeleteModal').modal('toggle');
      $('#btnAddJob').show();
      $('#btnEditJob').hide();
      $('#btnDeleteJo').hide();

      noty({
          type: 'error',
          layout: 'bottomRight',
          timeout: 3000,
          text: '<h4><center>Job Title(s) successfully deactivated!</center></h4>',
        });
    },
    error: function(result) {
        alert('error');
    }
  });
});

});
