<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTermTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tblpaymentterm', function (Blueprint $table) {
        $table->string('strPaymentTermID')->unique();
        $table->string('strPaymentTermName')->unique();
        $table->string('strPaymentTermDesc');
        $table->string('strStatus');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblpaymentterm');
    }
}
