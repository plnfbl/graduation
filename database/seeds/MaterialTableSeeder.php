<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class MaterialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('tblmaterial')->insert([
          'strMaterialID' => 'MAT00001',
          'strMaterialName' => 'Zircon Sand', 
          'intReorderLevel' => 24, 
          'intReorderQty' => 100, 
          'strUOMID' => 'U00001', 
          'strMaterialDesc' => 'investment casting special zircon sand',
          'strStatus' => 'Active'
      ]);

    }
}
