<?php

use Illuminate\Database\Seeder;

class QuoteRequestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tblquoterequest')->insert([
          'strQuoteRequestID' => 'QR00001',
          'strCompanyName' => 'Toyota Philippines',
          'strStreet' => '23-A',
          'strBrgy' => 'Mabuhay',
          'strCity' => 'Pasig',
          'strContactPerson' => 'Ronald Santos',
          'strContactNo' => '09345678756',
          'strStatus' => 'Approved',
      ]);
    }
}
